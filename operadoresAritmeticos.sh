#!/bin/bash

#Programa para ver TODOS los operadores aritmeticos
#Autor: Cristian Cabello

numA=10
numB=4

echo "Operadores aritmeticos"
echo "Los numeros son: A= $numA y B= $numB"
echo "Sumar A + B: " $((numA + numB))
echo "Restar A - B: " $((numA - numB))
echo "Multiplicar A * B: " $((numA * numB))
echo "Dividir A / B: " $((numA / numB))
echo "Residuo A % B: " $((numA % numB))

echo -e "\nOperadores relacionales"
echo "Los numeros son: A= $numA y B= $numB"
echo "A > B: " $((numA > numB))
echo "A < B: " $((numA < numB))
echo "A >= B: " $((numA >= numB))
echo "A <= B: " $((numA <= numB))
echo "A == B: " $((numA == numB))
echo "A != B: " $((numA != numB))

echo -e "\nOperadores de asignacion"
echo "Los numeros son: A= $numA y B= $numB"
echo "Sumar A += B: " $((numA += numB))
echo "Restar A -= B: " $((numA -= numB))
echo "Multiplicar A *= B: " $((numA *= numB))
echo "Dividir A /= B: " $((numA /= numB))
echo "Residuo A %= B: " $((numA %= numB))
