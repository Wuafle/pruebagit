#!/bin/bash
#Programa para ver informacion del Kernel
#Autor: Cristian Cabello

ubicacionActual=`pwd`
infoKernel=$(uname -a)

echo "La ubicación actual es la siguiente: $ubicacionActual"
echo "La información del Kernel es: $infoKernel"
